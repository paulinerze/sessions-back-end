PGDMP     +    &            
    w            conference_app    11.5    11.5 g    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                        0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    16384    conference_app    DATABASE     ~   CREATE DATABASE conference_app WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
    DROP DATABASE conference_app;
             postgres    false            �            1259    16432    attendee_tickets    TABLE     �   CREATE TABLE public.attendee_tickets (
    attendee_ticket_id integer NOT NULL,
    attendee_id integer NOT NULL,
    ticket_price_id integer NOT NULL,
    discount_code_id integer,
    net_price numeric(8,2) NOT NULL
);
 $   DROP TABLE public.attendee_tickets;
       public         postgres    false            �            1259    16430 '   attendee_tickets_attendee_ticket_id_seq    SEQUENCE     �   CREATE SEQUENCE public.attendee_tickets_attendee_ticket_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public.attendee_tickets_attendee_ticket_id_seq;
       public       postgres    false    205                       0    0 '   attendee_tickets_attendee_ticket_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public.attendee_tickets_attendee_ticket_id_seq OWNED BY public.attendee_tickets.attendee_ticket_id;
            public       postgres    false    204            �            1259    16388 	   attendees    TABLE     7  CREATE TABLE public.attendees (
    attendee_id integer NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    title character varying(40),
    company character varying(50),
    email character varying(80) NOT NULL,
    phone_number character varying(20)
);
    DROP TABLE public.attendees;
       public         postgres    false            �            1259    16386    attendees_attendee_id_seq    SEQUENCE     �   CREATE SEQUENCE public.attendees_attendee_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.attendees_attendee_id_seq;
       public       postgres    false    197                       0    0    attendees_attendee_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.attendees_attendee_id_seq OWNED BY public.attendees.attendee_id;
            public       postgres    false    196            �            1259    16424    discount_codes    TABLE       CREATE TABLE public.discount_codes (
    discount_code_id integer NOT NULL,
    discount_code character varying(20) NOT NULL,
    discount_name character varying(30) NOT NULL,
    discount_type character varying(1) NOT NULL,
    discount_amount numeric(8,2) NOT NULL
);
 "   DROP TABLE public.discount_codes;
       public         postgres    false            �            1259    16422 #   discount_codes_discount_code_id_seq    SEQUENCE     �   CREATE SEQUENCE public.discount_codes_discount_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.discount_codes_discount_code_id_seq;
       public       postgres    false    203                       0    0 #   discount_codes_discount_code_id_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.discount_codes_discount_code_id_seq OWNED BY public.discount_codes.discount_code_id;
            public       postgres    false    202            �            1259    16399    pricing_categories    TABLE     �   CREATE TABLE public.pricing_categories (
    pricing_category_code character varying(1) NOT NULL,
    pricing_category_name character varying(20) NOT NULL,
    pricing_start_date date NOT NULL,
    pricing_end_date date NOT NULL
);
 &   DROP TABLE public.pricing_categories;
       public         postgres    false            �            1259    16475    session_schedule    TABLE     �   CREATE TABLE public.session_schedule (
    schedule_id integer NOT NULL,
    time_slot_id integer NOT NULL,
    session_id integer NOT NULL,
    room character varying(30) NOT NULL
);
 $   DROP TABLE public.session_schedule;
       public         postgres    false            �            1259    16473     session_schedule_schedule_id_seq    SEQUENCE     �   CREATE SEQUENCE public.session_schedule_schedule_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.session_schedule_schedule_id_seq;
       public       postgres    false    211                       0    0     session_schedule_schedule_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.session_schedule_schedule_id_seq OWNED BY public.session_schedule.schedule_id;
            public       postgres    false    210            �            1259    16523    session_speakers    TABLE     k   CREATE TABLE public.session_speakers (
    session_id integer NOT NULL,
    speaker_id integer NOT NULL
);
 $   DROP TABLE public.session_speakers;
       public         postgres    false            �            1259    16499    session_tags    TABLE     c   CREATE TABLE public.session_tags (
    session_id integer NOT NULL,
    tag_id integer NOT NULL
);
     DROP TABLE public.session_tags;
       public         postgres    false            �            1259    16464    sessions    TABLE     �   CREATE TABLE public.sessions (
    session_id integer NOT NULL,
    session_name character varying(80) NOT NULL,
    session_description character varying(1024) NOT NULL,
    session_length integer NOT NULL
);
    DROP TABLE public.sessions;
       public         postgres    false            �            1259    16462    sessions_session_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sessions_session_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.sessions_session_id_seq;
       public       postgres    false    209                       0    0    sessions_session_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.sessions_session_id_seq OWNED BY public.sessions.session_id;
            public       postgres    false    208            �            1259    16514    speakers    TABLE     @  CREATE TABLE public.speakers (
    speaker_id integer NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    title character varying(40) NOT NULL,
    company character varying(50) NOT NULL,
    speaker_bio character varying(2000) NOT NULL,
    speaker_photo bytea
);
    DROP TABLE public.speakers;
       public         postgres    false            �            1259    16512    speakers_speaker_id_seq    SEQUENCE     �   CREATE SEQUENCE public.speakers_speaker_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.speakers_speaker_id_seq;
       public       postgres    false    216                       0    0    speakers_speaker_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.speakers_speaker_id_seq OWNED BY public.speakers.speaker_id;
            public       postgres    false    215            �            1259    16493    tags    TABLE     j   CREATE TABLE public.tags (
    tag_id integer NOT NULL,
    description character varying(30) NOT NULL
);
    DROP TABLE public.tags;
       public         postgres    false            �            1259    16491    tags_tag_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tags_tag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.tags_tag_id_seq;
       public       postgres    false    213                       0    0    tags_tag_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.tags_tag_id_seq OWNED BY public.tags.tag_id;
            public       postgres    false    212            �            1259    16406    ticket_prices    TABLE     �   CREATE TABLE public.ticket_prices (
    ticket_price_id integer NOT NULL,
    ticket_type_code character varying(1) NOT NULL,
    pricing_category_code character varying(1) NOT NULL,
    base_price numeric(8,2) NOT NULL
);
 !   DROP TABLE public.ticket_prices;
       public         postgres    false            �            1259    16404 !   ticket_prices_ticket_price_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ticket_prices_ticket_price_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.ticket_prices_ticket_price_id_seq;
       public       postgres    false    201            	           0    0 !   ticket_prices_ticket_price_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.ticket_prices_ticket_price_id_seq OWNED BY public.ticket_prices.ticket_price_id;
            public       postgres    false    200            �            1259    16394    ticket_types    TABLE     �   CREATE TABLE public.ticket_types (
    ticket_type_code character varying(1) NOT NULL,
    ticket_type_name character varying(30) NOT NULL,
    description character varying(100) NOT NULL,
    includes_workshop boolean NOT NULL
);
     DROP TABLE public.ticket_types;
       public         postgres    false            �            1259    16455 
   time_slots    TABLE     �   CREATE TABLE public.time_slots (
    time_slot_id integer NOT NULL,
    time_slot_date date NOT NULL,
    start_time time without time zone NOT NULL,
    end_time time without time zone NOT NULL,
    is_keynote_time_slot boolean DEFAULT false NOT NULL
);
    DROP TABLE public.time_slots;
       public         postgres    false            �            1259    16453    time_slots_time_slot_id_seq    SEQUENCE     �   CREATE SEQUENCE public.time_slots_time_slot_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.time_slots_time_slot_id_seq;
       public       postgres    false    207            
           0    0    time_slots_time_slot_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.time_slots_time_slot_id_seq OWNED BY public.time_slots.time_slot_id;
            public       postgres    false    206            �            1259    16560    workshop_registrations    TABLE     z   CREATE TABLE public.workshop_registrations (
    workshop_id integer NOT NULL,
    attendee_ticket_id integer NOT NULL
);
 *   DROP TABLE public.workshop_registrations;
       public         postgres    false            �            1259    16547    workshop_speakers    TABLE     m   CREATE TABLE public.workshop_speakers (
    workshop_id integer NOT NULL,
    speaker_id integer NOT NULL
);
 %   DROP TABLE public.workshop_speakers;
       public         postgres    false            �            1259    16538 	   workshops    TABLE     #  CREATE TABLE public.workshops (
    workshop_id integer NOT NULL,
    workshop_name character varying(60) NOT NULL,
    description character varying(1024) NOT NULL,
    requirements character varying(1024) NOT NULL,
    room character varying(30) NOT NULL,
    capacity integer NOT NULL
);
    DROP TABLE public.workshops;
       public         postgres    false            �            1259    16536    workshops_workshop_id_seq    SEQUENCE     �   CREATE SEQUENCE public.workshops_workshop_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.workshops_workshop_id_seq;
       public       postgres    false    219                       0    0    workshops_workshop_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.workshops_workshop_id_seq OWNED BY public.workshops.workshop_id;
            public       postgres    false    218            :           2604    16435 #   attendee_tickets attendee_ticket_id    DEFAULT     �   ALTER TABLE ONLY public.attendee_tickets ALTER COLUMN attendee_ticket_id SET DEFAULT nextval('public.attendee_tickets_attendee_ticket_id_seq'::regclass);
 R   ALTER TABLE public.attendee_tickets ALTER COLUMN attendee_ticket_id DROP DEFAULT;
       public       postgres    false    204    205    205            7           2604    16391    attendees attendee_id    DEFAULT     ~   ALTER TABLE ONLY public.attendees ALTER COLUMN attendee_id SET DEFAULT nextval('public.attendees_attendee_id_seq'::regclass);
 D   ALTER TABLE public.attendees ALTER COLUMN attendee_id DROP DEFAULT;
       public       postgres    false    196    197    197            9           2604    16427    discount_codes discount_code_id    DEFAULT     �   ALTER TABLE ONLY public.discount_codes ALTER COLUMN discount_code_id SET DEFAULT nextval('public.discount_codes_discount_code_id_seq'::regclass);
 N   ALTER TABLE public.discount_codes ALTER COLUMN discount_code_id DROP DEFAULT;
       public       postgres    false    202    203    203            >           2604    16478    session_schedule schedule_id    DEFAULT     �   ALTER TABLE ONLY public.session_schedule ALTER COLUMN schedule_id SET DEFAULT nextval('public.session_schedule_schedule_id_seq'::regclass);
 K   ALTER TABLE public.session_schedule ALTER COLUMN schedule_id DROP DEFAULT;
       public       postgres    false    210    211    211            =           2604    16467    sessions session_id    DEFAULT     z   ALTER TABLE ONLY public.sessions ALTER COLUMN session_id SET DEFAULT nextval('public.sessions_session_id_seq'::regclass);
 B   ALTER TABLE public.sessions ALTER COLUMN session_id DROP DEFAULT;
       public       postgres    false    209    208    209            @           2604    16517    speakers speaker_id    DEFAULT     z   ALTER TABLE ONLY public.speakers ALTER COLUMN speaker_id SET DEFAULT nextval('public.speakers_speaker_id_seq'::regclass);
 B   ALTER TABLE public.speakers ALTER COLUMN speaker_id DROP DEFAULT;
       public       postgres    false    215    216    216            ?           2604    16496    tags tag_id    DEFAULT     j   ALTER TABLE ONLY public.tags ALTER COLUMN tag_id SET DEFAULT nextval('public.tags_tag_id_seq'::regclass);
 :   ALTER TABLE public.tags ALTER COLUMN tag_id DROP DEFAULT;
       public       postgres    false    213    212    213            8           2604    16409    ticket_prices ticket_price_id    DEFAULT     �   ALTER TABLE ONLY public.ticket_prices ALTER COLUMN ticket_price_id SET DEFAULT nextval('public.ticket_prices_ticket_price_id_seq'::regclass);
 L   ALTER TABLE public.ticket_prices ALTER COLUMN ticket_price_id DROP DEFAULT;
       public       postgres    false    201    200    201            ;           2604    16458    time_slots time_slot_id    DEFAULT     �   ALTER TABLE ONLY public.time_slots ALTER COLUMN time_slot_id SET DEFAULT nextval('public.time_slots_time_slot_id_seq'::regclass);
 F   ALTER TABLE public.time_slots ALTER COLUMN time_slot_id DROP DEFAULT;
       public       postgres    false    206    207    207            A           2604    16541    workshops workshop_id    DEFAULT     ~   ALTER TABLE ONLY public.workshops ALTER COLUMN workshop_id SET DEFAULT nextval('public.workshops_workshop_id_seq'::regclass);
 D   ALTER TABLE public.workshops ALTER COLUMN workshop_id DROP DEFAULT;
       public       postgres    false    219    218    219            �          0    16432    attendee_tickets 
   TABLE DATA               y   COPY public.attendee_tickets (attendee_ticket_id, attendee_id, ticket_price_id, discount_code_id, net_price) FROM stdin;
    public       postgres    false    205   �       �          0    16388 	   attendees 
   TABLE DATA               l   COPY public.attendees (attendee_id, first_name, last_name, title, company, email, phone_number) FROM stdin;
    public       postgres    false    197   ��       �          0    16424    discount_codes 
   TABLE DATA               x   COPY public.discount_codes (discount_code_id, discount_code, discount_name, discount_type, discount_amount) FROM stdin;
    public       postgres    false    203   ��       �          0    16399    pricing_categories 
   TABLE DATA               �   COPY public.pricing_categories (pricing_category_code, pricing_category_name, pricing_start_date, pricing_end_date) FROM stdin;
    public       postgres    false    199   ւ       �          0    16475    session_schedule 
   TABLE DATA               W   COPY public.session_schedule (schedule_id, time_slot_id, session_id, room) FROM stdin;
    public       postgres    false    211   <�       �          0    16523    session_speakers 
   TABLE DATA               B   COPY public.session_speakers (session_id, speaker_id) FROM stdin;
    public       postgres    false    217   ��       �          0    16499    session_tags 
   TABLE DATA               :   COPY public.session_tags (session_id, tag_id) FROM stdin;
    public       postgres    false    214   ��       �          0    16464    sessions 
   TABLE DATA               a   COPY public.sessions (session_id, session_name, session_description, session_length) FROM stdin;
    public       postgres    false    209   ��       �          0    16514    speakers 
   TABLE DATA               q   COPY public.speakers (speaker_id, first_name, last_name, title, company, speaker_bio, speaker_photo) FROM stdin;
    public       postgres    false    216   #�       �          0    16493    tags 
   TABLE DATA               3   COPY public.tags (tag_id, description) FROM stdin;
    public       postgres    false    213   h�       �          0    16406    ticket_prices 
   TABLE DATA               m   COPY public.ticket_prices (ticket_price_id, ticket_type_code, pricing_category_code, base_price) FROM stdin;
    public       postgres    false    201   ��       �          0    16394    ticket_types 
   TABLE DATA               j   COPY public.ticket_types (ticket_type_code, ticket_type_name, description, includes_workshop) FROM stdin;
    public       postgres    false    198   P�       �          0    16455 
   time_slots 
   TABLE DATA               n   COPY public.time_slots (time_slot_id, time_slot_date, start_time, end_time, is_keynote_time_slot) FROM stdin;
    public       postgres    false    207   ��       �          0    16560    workshop_registrations 
   TABLE DATA               Q   COPY public.workshop_registrations (workshop_id, attendee_ticket_id) FROM stdin;
    public       postgres    false    221   ��       �          0    16547    workshop_speakers 
   TABLE DATA               D   COPY public.workshop_speakers (workshop_id, speaker_id) FROM stdin;
    public       postgres    false    220   ��       �          0    16538 	   workshops 
   TABLE DATA               j   COPY public.workshops (workshop_id, workshop_name, description, requirements, room, capacity) FROM stdin;
    public       postgres    false    219   ʑ                  0    0 '   attendee_tickets_attendee_ticket_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.attendee_tickets_attendee_ticket_id_seq', 1, true);
            public       postgres    false    204                       0    0    attendees_attendee_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.attendees_attendee_id_seq', 1, true);
            public       postgres    false    196                       0    0 #   discount_codes_discount_code_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.discount_codes_discount_code_id_seq', 1, true);
            public       postgres    false    202                       0    0     session_schedule_schedule_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.session_schedule_schedule_id_seq', 92, true);
            public       postgres    false    210                       0    0    sessions_session_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.sessions_session_id_seq', 93, true);
            public       postgres    false    208                       0    0    speakers_speaker_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.speakers_speaker_id_seq', 41, true);
            public       postgres    false    215                       0    0    tags_tag_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.tags_tag_id_seq', 13, true);
            public       postgres    false    212                       0    0 !   ticket_prices_ticket_price_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.ticket_prices_ticket_price_id_seq', 10, true);
            public       postgres    false    200                       0    0    time_slots_time_slot_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.time_slots_time_slot_id_seq', 13, true);
            public       postgres    false    206                       0    0    workshops_workshop_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.workshops_workshop_id_seq', 13, true);
            public       postgres    false    218            M           2606    16437 &   attendee_tickets attendee_tickets_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.attendee_tickets
    ADD CONSTRAINT attendee_tickets_pkey PRIMARY KEY (attendee_ticket_id);
 P   ALTER TABLE ONLY public.attendee_tickets DROP CONSTRAINT attendee_tickets_pkey;
       public         postgres    false    205            C           2606    16393    attendees attendees_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.attendees
    ADD CONSTRAINT attendees_pkey PRIMARY KEY (attendee_id);
 B   ALTER TABLE ONLY public.attendees DROP CONSTRAINT attendees_pkey;
       public         postgres    false    197            K           2606    16429 "   discount_codes discount_codes_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.discount_codes
    ADD CONSTRAINT discount_codes_pkey PRIMARY KEY (discount_code_id);
 L   ALTER TABLE ONLY public.discount_codes DROP CONSTRAINT discount_codes_pkey;
       public         postgres    false    203            G           2606    16403 *   pricing_categories pricing_categories_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.pricing_categories
    ADD CONSTRAINT pricing_categories_pkey PRIMARY KEY (pricing_category_code);
 T   ALTER TABLE ONLY public.pricing_categories DROP CONSTRAINT pricing_categories_pkey;
       public         postgres    false    199            S           2606    16480 &   session_schedule session_schedule_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.session_schedule
    ADD CONSTRAINT session_schedule_pkey PRIMARY KEY (schedule_id);
 P   ALTER TABLE ONLY public.session_schedule DROP CONSTRAINT session_schedule_pkey;
       public         postgres    false    211            Q           2606    16472    sessions sessions_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (session_id);
 @   ALTER TABLE ONLY public.sessions DROP CONSTRAINT sessions_pkey;
       public         postgres    false    209            W           2606    16522    speakers speakers_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.speakers
    ADD CONSTRAINT speakers_pkey PRIMARY KEY (speaker_id);
 @   ALTER TABLE ONLY public.speakers DROP CONSTRAINT speakers_pkey;
       public         postgres    false    216            U           2606    16498    tags tags_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (tag_id);
 8   ALTER TABLE ONLY public.tags DROP CONSTRAINT tags_pkey;
       public         postgres    false    213            I           2606    16411     ticket_prices ticket_prices_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.ticket_prices
    ADD CONSTRAINT ticket_prices_pkey PRIMARY KEY (ticket_price_id);
 J   ALTER TABLE ONLY public.ticket_prices DROP CONSTRAINT ticket_prices_pkey;
       public         postgres    false    201            E           2606    16398    ticket_types ticket_types_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.ticket_types
    ADD CONSTRAINT ticket_types_pkey PRIMARY KEY (ticket_type_code);
 H   ALTER TABLE ONLY public.ticket_types DROP CONSTRAINT ticket_types_pkey;
       public         postgres    false    198            O           2606    16461    time_slots time_slots_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.time_slots
    ADD CONSTRAINT time_slots_pkey PRIMARY KEY (time_slot_id);
 D   ALTER TABLE ONLY public.time_slots DROP CONSTRAINT time_slots_pkey;
       public         postgres    false    207            Y           2606    16546    workshops workshops_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.workshops
    ADD CONSTRAINT workshops_pkey PRIMARY KEY (workshop_id);
 B   ALTER TABLE ONLY public.workshops DROP CONSTRAINT workshops_pkey;
       public         postgres    false    219            \           2606    16438 2   attendee_tickets attendee_tickets_attendee_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.attendee_tickets
    ADD CONSTRAINT attendee_tickets_attendee_id_fkey FOREIGN KEY (attendee_id) REFERENCES public.attendees(attendee_id);
 \   ALTER TABLE ONLY public.attendee_tickets DROP CONSTRAINT attendee_tickets_attendee_id_fkey;
       public       postgres    false    2883    205    197            ^           2606    16448 7   attendee_tickets attendee_tickets_discount_code_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.attendee_tickets
    ADD CONSTRAINT attendee_tickets_discount_code_id_fkey FOREIGN KEY (discount_code_id) REFERENCES public.discount_codes(discount_code_id);
 a   ALTER TABLE ONLY public.attendee_tickets DROP CONSTRAINT attendee_tickets_discount_code_id_fkey;
       public       postgres    false    2891    205    203            ]           2606    16443 6   attendee_tickets attendee_tickets_ticket_price_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.attendee_tickets
    ADD CONSTRAINT attendee_tickets_ticket_price_id_fkey FOREIGN KEY (ticket_price_id) REFERENCES public.ticket_prices(ticket_price_id);
 `   ALTER TABLE ONLY public.attendee_tickets DROP CONSTRAINT attendee_tickets_ticket_price_id_fkey;
       public       postgres    false    2889    205    201            `           2606    16486 1   session_schedule session_schedule_session_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.session_schedule
    ADD CONSTRAINT session_schedule_session_id_fkey FOREIGN KEY (session_id) REFERENCES public.sessions(session_id);
 [   ALTER TABLE ONLY public.session_schedule DROP CONSTRAINT session_schedule_session_id_fkey;
       public       postgres    false    209    2897    211            _           2606    16481 3   session_schedule session_schedule_time_slot_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.session_schedule
    ADD CONSTRAINT session_schedule_time_slot_id_fkey FOREIGN KEY (time_slot_id) REFERENCES public.time_slots(time_slot_id);
 ]   ALTER TABLE ONLY public.session_schedule DROP CONSTRAINT session_schedule_time_slot_id_fkey;
       public       postgres    false    2895    207    211            c           2606    16526 1   session_speakers session_speakers_session_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.session_speakers
    ADD CONSTRAINT session_speakers_session_id_fkey FOREIGN KEY (session_id) REFERENCES public.sessions(session_id);
 [   ALTER TABLE ONLY public.session_speakers DROP CONSTRAINT session_speakers_session_id_fkey;
       public       postgres    false    2897    209    217            d           2606    16531 1   session_speakers session_speakers_speaker_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.session_speakers
    ADD CONSTRAINT session_speakers_speaker_id_fkey FOREIGN KEY (speaker_id) REFERENCES public.speakers(speaker_id);
 [   ALTER TABLE ONLY public.session_speakers DROP CONSTRAINT session_speakers_speaker_id_fkey;
       public       postgres    false    2903    217    216            a           2606    16502 )   session_tags session_tags_session_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.session_tags
    ADD CONSTRAINT session_tags_session_id_fkey FOREIGN KEY (session_id) REFERENCES public.sessions(session_id);
 S   ALTER TABLE ONLY public.session_tags DROP CONSTRAINT session_tags_session_id_fkey;
       public       postgres    false    2897    214    209            b           2606    16507 %   session_tags session_tags_tag_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.session_tags
    ADD CONSTRAINT session_tags_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES public.tags(tag_id);
 O   ALTER TABLE ONLY public.session_tags DROP CONSTRAINT session_tags_tag_id_fkey;
       public       postgres    false    2901    213    214            [           2606    16417 6   ticket_prices ticket_prices_pricing_category_code_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ticket_prices
    ADD CONSTRAINT ticket_prices_pricing_category_code_fkey FOREIGN KEY (pricing_category_code) REFERENCES public.pricing_categories(pricing_category_code);
 `   ALTER TABLE ONLY public.ticket_prices DROP CONSTRAINT ticket_prices_pricing_category_code_fkey;
       public       postgres    false    201    2887    199            Z           2606    16412 1   ticket_prices ticket_prices_ticket_type_code_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ticket_prices
    ADD CONSTRAINT ticket_prices_ticket_type_code_fkey FOREIGN KEY (ticket_type_code) REFERENCES public.ticket_types(ticket_type_code);
 [   ALTER TABLE ONLY public.ticket_prices DROP CONSTRAINT ticket_prices_ticket_type_code_fkey;
       public       postgres    false    2885    201    198            h           2606    16568 E   workshop_registrations workshop_registrations_attendee_ticket_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.workshop_registrations
    ADD CONSTRAINT workshop_registrations_attendee_ticket_id_fkey FOREIGN KEY (attendee_ticket_id) REFERENCES public.attendee_tickets(attendee_ticket_id);
 o   ALTER TABLE ONLY public.workshop_registrations DROP CONSTRAINT workshop_registrations_attendee_ticket_id_fkey;
       public       postgres    false    221    205    2893            g           2606    16563 >   workshop_registrations workshop_registrations_workshop_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.workshop_registrations
    ADD CONSTRAINT workshop_registrations_workshop_id_fkey FOREIGN KEY (workshop_id) REFERENCES public.workshops(workshop_id);
 h   ALTER TABLE ONLY public.workshop_registrations DROP CONSTRAINT workshop_registrations_workshop_id_fkey;
       public       postgres    false    2905    221    219            f           2606    16555 3   workshop_speakers workshop_speakers_speaker_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.workshop_speakers
    ADD CONSTRAINT workshop_speakers_speaker_id_fkey FOREIGN KEY (speaker_id) REFERENCES public.speakers(speaker_id);
 ]   ALTER TABLE ONLY public.workshop_speakers DROP CONSTRAINT workshop_speakers_speaker_id_fkey;
       public       postgres    false    220    2903    216            e           2606    16550 4   workshop_speakers workshop_speakers_workshop_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.workshop_speakers
    ADD CONSTRAINT workshop_speakers_workshop_id_fkey FOREIGN KEY (workshop_id) REFERENCES public.workshops(workshop_id);
 ^   ALTER TABLE ONLY public.workshop_speakers DROP CONSTRAINT workshop_speakers_workshop_id_fkey;
       public       postgres    false    219    220    2905            �      x������ � �      �      x������ � �      �      x������ � �      �   V   x�s�tM,ʩTp�,J�420��54�502�����)WgPjziNbB��4�52����I,.Q���+-IEH��0�50����� c��      �   o  x�U��N�@F�3O��C)Ke�Wn�4�D#2�������i�49��ᤔ*T\��p>�=�Ӕ�w����x�؄64~n��ϛ���s���y���f��h��o���z��*�����?�i��J �"@Vu��JY�fWȪ&�Z�
d�,��nK^��i��������������%AW7��� 7ˁ���?�����l�n�n�nV]U�TfkUܬ���|���UX��vu5��U�.�*��dߨ,n}0�
�>]�����1v��NPNJ�d|�o�`Xnj�lPL[��r�xXl�����0|�i����:�����:���U�u��l_*˰S/X����~c�=��      �   �   x���D1ϸ�U��@/��:�pAh�@�80	�\�(��E�<1E5�z�2�&�b1ή�As�����6�v&b:k��`.㩆Q$��0�Lג���N1�6���ɂ�$Y�ߒ���\4:�0Q�<�'��H)�۶��!�:�4�@�l$�B�td.KjIO��N-�����V~k��&��E��=�>3m�� ���:~      �      x������ � �      �   c  x��VKs�6>ÿ39�=أ��\:�öb)R,%���Q��X<�a~}wRU'=�]�����h�'^(�8���=�Zn��ɎS��K�uo�pB:ݫI�;�}au�&iʭ�#�}nO��j�5���2�<�#q�t���4��a�萁r�G���j�%M��p��[<��
���V��+]:f�Q�v&R�-7ɞ���U���~CFd܄?���@kG����R�qKH�quGV��vI�K��|�E�A^�&io�0��d���
L���$X������	]��R�6te��2j��{��5LB�z�er�����6��W<u��nK�tktL�'��PѸ����*��p����3���;O����s|��Dzd9�NF�Z��\���y7�КI�k�r1��b��!簧h�'_-�1f��i }�M4/�M���䈯�n���.��� ��p�܁:.!޽����l���$Cə�.m��${Jb�\ꜛ��U�Sf`��)��&�����
�~����s:Ô=)�q�X:t��f�s�P",�@τ��1噌�ݲ��<��nϣ"��'�[2�ڍBx^��k&��)����s�O��rG�	�@��F�w��n����]�7A\�ܯp�@�`<�p+d	W۟��m���\m�'
�ݭ�ܫ���$��~���US�Ĥ{� �7��Cc�ɢW���3��V<�+�a��!��0;�M��3�.~ Ҩ�g��{���%�fA?�,=)�F���Q��=��B�{����r(�e�oO��"�>tX��2�=`�JR��e�T����lH�������;�{.uhzpA���J~@8'�<��i����RX�)8A�Ri�$���X%I�8���ٵ/|]�&.,�;�����j�X�;kY��:�\+ѷ�ҥK�\aa��H����<lhtZH��,����m�m��h�`sY@���T�p:>��#6�CعAWlw�n9%p�n(6-�]]�e�6�W�)XmP_�p��@�@�{&T�\��Nm|,֘Zܾ�Q¡Y�u�>�`���g���]\9t�d������_����e���d ��rfT�<�S�D��{Yց�!&����gLI���,���p2*��hp	K��~F���,R��נ��"����$�._�|S�ʏՠm��xC�6,X�[��e'p_�(<ϜIY�_H�j"���a�QP%�r܅:��^V����\e9�pfO6v/�4�^�*���0�J��?�E�Sь�D��6�0�V񙱄�}dk�,�<�o�1���F���	W��fH���u�����c�������*�e^�������I�ѷI�Qx�=1�f*�}suu�/�J��      �   5  x��V�v�:\w�B_0g0y.�	'p��l[`��e8��o�G's�
0RwWuu�{4�n�,d�����N��Vj���DE�>G���ٕ߱�4����O'=p*s۝�{�Y]xeM.B%����/��p*�M\��)����V[��n��V���t�S�lE���X��h#�N5�O��)��&�6��f�b.�Ĩ��K��˒�k���3z�}jMLw��g�C��bjw�	6��֨ĩ��N3N�8�[�W��!�q���S6^E���B{e���z @<��64u�D*C��f��`�f-�X�;�e6��Ӣ3�%�-�1-�֊Ӽ�3	��m}��g��]F�
�4�prgݦiT�'��J�i��{�d�Ov
L����z=�[�gn#��q�,�@�ň=w����"�%M�s{�(�1aà���X,T,�}���Z���0�>��u)�ykċ�%���)M�{E��0e�j���<{�4[OA]+��&�t�8�X:hC4�J�h�@$ŭ�E�9��4����Z�b�9f�\���,a��ꏗ&�0�4z_ƪ�_b���#����&�%`�az�2�%�Z�׻B�<�yg�E��RA8�o�� gC������ B,+&^·�%��o�b��w�z4�:�h&.7L@����j���B@��&��0i�s:'���8�7ʘ������0��ЃMLnM���t6���1��78�G�7�2�x��t=o���	�s�`�z��4+���!�9~p���4���N �NrpI�����9�78m*����x��X���������a蠳N�u�UQ{�4i��+I�+(f�X����5�&D�B$�&�Jt�+�<>8Ng�|���>-ULe7ı��������`á1���3����Ni���e�Q�6ۭ���!V��V�1/[1��N�xT�����P/�jP�j%{��po|*�0Ը��E�0y^/���\3ӈw%�p�4�gv��.sPټ�Ko�D�dz���NC����T��K@�V���n�����ל�����M�> {����=�VH��)�-f`E�M������vM      �   x   x��;!���U�"�$���1�� �J�<Lܽ�����`�ގ�?��ӿ�/�%���mM�p�Cp^������9\��)�����ǰ�.#��)������)�/��Y�"�      �   P   x�=�1�0C��9L�����e����ρ� �'}۰pa�U�D�4�(��Ԇ�aOt"0;��j�dg�I���p��SD��Y�      �   �   x���1�0E��>@�; .P�+Kp]%jcG���d �����{orS��Zv'DR�(�R%F��BٛB0#^�"�]�Q
�
��
%!����f�����nt��}O�:���8�R�AK�(�W�1]��'Įr�?����|���.���	A�ac      �   �   x�U�A� �u�K'|�v�,����FEIX���!,|���΅�ͻ���C�� �X*� P`�J��;�@g�y���
�;�k&ܞ�u ^o�;���ݿ��h�]��!�f�.!A�z���~"�T�H.      �      x������ � �      �      x������ � �      �   u  x�E��n1F�g�b� -$�ϥI�R	�&KU��������K�y���J�������*
�b�c[܉��g���r�[ޒ�M����
��i�2��i�y���k��E��^��O��)�8�W����輯/�(y�Y�\�|p~��W�J�,���\9`�Do�79q������a��R,ر좼R���!\�{g�Q�\�����s��Q4HƧ\W���f�����n�
dE�${nI�<��A��w����V�GU�&��7-���g�$5>��BR.�r	C���Bȏj����L��]L^�+9m��!Ժ��E����`K2w���O��6�<<���/w�O����4}7����mm��c��{�4�?I�H     